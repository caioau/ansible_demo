# Setup do gitlab-runner com docker

Vamos utilizar um setup docker-in-docker, com [docker-compose](docker-compose.yml)

Suba os containers: `docker compose up -d`

Na barra a esquerda, na interface web do gitlab, clique em Settings -> CI/CD e em runners copie o token gerado.

Abra um shell no container do runner: `docker exec -it gitlab-runner bash`

E substitua as variáveis da URL e do token e execute o comando abaixo:

```bash
export GITLAB_URL=https://gitlab.com/
export REGISTRATION_TOKEN=your-token
gitlab-runner --debug register \
    --non-interactive \
    --url $GITLAB_URL \
    --registration-token $REGISTRATION_TOKEN \
    --executor docker \
    --description "runner-hostname" \
    --docker-image "docker:dind" \
    --run-untagged=true \
    --locked=false \
    --docker-privileged=true \
    --docker-tlsverify=false \
    --docker-host "tcp://dind:2375" \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --docker-volumes "/cache" \
    --docker-volumes "/builds:/builds" \
    --access-level "not_protected" 
```

Um problema que tive com esse setup do Molecule é quando estamos rodando o kernel com cgroupv2, precisamos subir o daemon do docker com a configuração `cgroupns=host` , se quiser ler mais veja esse issue do github [issue #4 em geerlingguy/docker-debian11-ansible](https://github.com/geerlingguy/docker-debian11-ansible/issues/4#issuecomment-1113675616)
