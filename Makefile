SHELL := /bin/bash

python-bin := python3.11

.PHONY: build ansible-lint lint


.ONESHELL:
build:
	${python-bin} -m venv .venv
	source .venv/bin/activate
	pip3 install -U pip wheel setuptools
	pip3 install -U -r requirements.txt
	pip3 --version
	python3 --version

ansible-lint:
	ansible-lint # ansible-lint options are defined on .ansible-lint

lint: ansible-lint
