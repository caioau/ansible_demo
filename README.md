# ansible-tutorial

Esse repositório tem como objetivo materializar os exemplos do texto sobre ansible publicado no medium pelo grupo computando-arte: [ansible localhost -a "/bin/echo Primeiros passos com o Ansible"](https://medium.com/computando-arte/ansible-localhost-a-bin-echo-primeiros-passos-com-o-ansible-7674529524c0), se preferir no formato [markdown](https://gitlab.com/caioau/caioau.gitlab.io/-/blob/master/content/blog/intro-ansible.md)

## Licença

O texto e os códigos aqui publicado estão sob a licença [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

## Introdução

Esse repositório tem como objetivo ser um passo-a-passo para ajudar quem está interessado pelo Ansible. Não deixe de ler o texto para as explicações completas ;)

Aqui vamos mostrar como preparar um ambiente para rodar o Ansible, subindo algumas maquinas virtuais com o Vagrant, um playbook simples de exemplo e por fim usando o [Molecule](https://molecule.readthedocs.io/) com o intuito de criar testes do playbook (e como usar no CI do GitLab).

## Instalando tudo

Para rodar tudo desse repo você precisa instalar:

- [ ] Python para rodar o Ansible
- [ ] sshpass para acessar o ssh com senha (não apenas chave) (`sudo apt install sshpass` no linux e `brew install hudochenkov/sshpass/sshpass` no mac)
- [ ] Vagrant e VirtualBox para criar VMs com facilidade: [guia de instalação](https://www.vagrantup.com/docs/installation)
- [ ] Docker para rodar os testes com o Molecule: [guia de instalação](https://docs.docker.com/engine/install/)

Instale as dependências listadas no [requirements.txt](requirements.txt) (`pip install -r requirements.txt`).

Testando se a instalação teve sucesso: `ansible -m ping localhost`

```
[WARNING]: No inventory was parsed, only implicit localhost is available
localhost | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

Se você obter a saída acima então deu bom.

## Subindo as VMs com Vagrant

O Vagrant é uma ferramenta que agiliza a criação de VMs, que ajuda muito a testar seus playbooks do Ansible.

Neste repo criamos 2 VMs no [Vagrantfile](Vagrantfile), um Debian 11 (codinome bullseye) e um Ubuntu 20.04 (focal).

Para criar as VMs: `vagrant up`, para acessar a vm: `vagrant ssh debvm` (onde debvm é o nome da VM, no nosso caso temos a debvm e focalvm).

Para desligar as VMs, sem deletar os dados: `vagrant halt`

E para destruir as VMs, apagando todos os dados: `vagrant destroy`

Tem várias outras opções que podem ser utilizadas no `Vagrantfile`, por exemplo disparar o Ansible quando as VMs são criadas, se tiver interesse consulte a documentação: [Vagrantfile docs](https://www.vagrantup.com/docs/vagrantfile).

Com as VMs rodando, vamos testar usar o Ansible. O nosso arquivo inventário é o [inventario.ini](inventario.ini), onde definimos os mesmos IPs que o Vagrantfile.

`ansible -i inventario.ini -m ping all`

```
deb11 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
rocky8 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

Funcionou! não precisamos passar o inventário toda fez, pois o mesmo está definido no [ansible.cfg](./ansible.cfg), basta fazer `ansible -m ping all`

## Rodando comandos adhoc

No Ansible as tarefas são executas através de módulos.

Se quisermos executar um comando podemos usar o módulo `command`, por exemplo se quisermos executar hostnamectl em todas as maquinas:

`ansible -m command -a hostnamectl all`

```
deb11 | CHANGED | rc=0 >>
   Static hostname: debian
         Icon name: computer-vm
           Chassis: vm
        Machine ID: c4d0559486ea4fcbbe63fb0ecbff93dd
           Boot ID: 3c1dab9e33cb4573886119ae45cb243b
    Virtualization: oracle
  Operating System: Debian GNU/Linux 11 (bullseye)
            Kernel: Linux 5.10.0-13-amd64
      Architecture: x86-64
rocky8 | CHANGED | rc=0 >>
   Static hostname: rockylinux
         Icon name: computer-vm
           Chassis: vm
        Machine ID: bdf16db6bc854ea7a441106d24ed2d6e
           Boot ID: 39577c2675a44e2996b6553931b5fc9c
    Virtualization: oracle
  Operating System: Rocky Linux 8.5 (Green Obsidian)
       CPE OS Name: cpe:/o:rocky:rocky:8:GA
            Kernel: Linux 4.18.0-348.12.2.el8_5.x86_64
      Architecture: x86-64
```

## Utilizando playbooks

No nosso exemplo vamos hospedar um site estático criado com o [Hugo](https://gohugo.io/) e utilizando o nginx como webserver.

O playbook está no [playbook.yml](playbook.yml), onde definimos o repositorio do site que vamos hospedar e outras configurações, chamando o role [hugo_nginx](roles/hugo_nginx/).

Para rodar o playbook: `ansible-playbook playbook.yaml`

Testando: `curl 192.168.60.6`

## Testes com molecule

Usamos o Vagrant para criar maquinas virtuais para testar manualmente nossos playbooks.

O Molecule é uma ferramenta que permite rodar testes automaticamente, utilizando o Docker.

Primeiro precisamos configurar o Molecule, veja o [molecule/default/molecule.yml](roles/hugo_nginx/molecule/default/molecule.yml), aqui apontamos a imagem docker da distribuição que vamos utilizar e definir que o ansible será utilizado para provisionar e testar.

Crie o [molecule/default/converge.yml](roles/hugo_nginx/molecule/default/converge.yml) definindo o que o Molecule vai rodar quando subir, vamos importar o nosso playbook.yml.

Por fim precisamos escrever os testes em [molecule/default/verify.yml](roles/hugo_nginx/molecule/default/verify.yml), no qual definimos alguns requests e testamos se a resposta é o que esperamos.

Para "subir" o contêiner, ou seja, criar o Docker e rodar o playbook: `molecule converge`

Para acessar o docker: `molecule login`
(não precisa fazer `docker exec -it <nome_container> bash` ^_^ )

Executar o teste: `molecule verify`

Para destruir: `molecule destroy`

Para fazer tudo (subir, verificar idempotência, executar o teste e destruir): `molecule test`

## setup do GitLab CI para executar os testes

Para testar e garantir a qualidade dos nossos playbook além do Molecule, podemos utilizar o yaml-lint para verificar que os yaml são válidos. E o ansible-lint para sempre seguir as melhores práticas do Ansible.

Isso tudo pode rodar no seu setup de integração continua, como fizemos no nosso [gitlab-ci.yml](.gitlab-ci.yml), dessa forma em cada commit podemos rodar essas verificações e testes.

## setup do runner do GitLab

Afim de executar as pipelines do CI, podemos utilizar o runners gratuidos, oferecidos pelo GitLab, com um limite de recursos. Porém como queremos rodar o Ansible para alterar nossa infra precisamos instalar um runner dentro da nossa rede.

Siga as instruções para instalar o runner estão em [gitlab-runner/README.md](gitlab-runner/README.md).

## Referências

Para aprender ansible, a melhor referência é o livro do Jeff Geerling, [Ansible for DevOps](https://www.ansiblefordevops.com/) o livro tem vários exemplos e dicas. Pra quem prefere o formato de vídeos, no início da pandemia o autor trouxe o conteúdo do livro em forma de lives: [Ansible 101 playlist](https://www.youtube.com/playlist?list=PL2_OBreMn7FqZkvMYt6ATmgC0KAGGJNAN). E pra quem prefere ir direto nos códigos os exemplos estão disponíveis nesse github: [github.com/geerlingguy/ansible-for-devops](https://github.com/geerlingguy/ansible-for-devops)
